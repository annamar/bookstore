CREATE SCHEMA IF NOT EXISTS `bookstore`;

USE `bookstore` ;

CREATE TABLE IF NOT EXISTS `bookstore`.`author` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `bookstore`.`book` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `price` FLOAT NOT NULL,
  `author_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `author_id`
    FOREIGN KEY (`author_id`)
    REFERENCES `bookstore`.`author` (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `bookstore`.`post` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `index` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `book_id` INT NOT NULL,
  PRIMARY KEY (`id`),
   CONSTRAINT `book_insert_book`
    FOREIGN KEY (`book_id`)
    REFERENCES `bookstore`.`book` (`id`)) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `bookstore`.`client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `post_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `client_post`
    FOREIGN KEY (`post_id`)
    REFERENCES `bookstore`.`post` (`id`)) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `bookstore`.`sale` (
  `id` INT(11) NOT NULL,
  `date` DATETIME NOT NULL,
  `count` INT(11) NOT NULL,
  `book_id` INT NOT NULL,
  `client_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `sale_book`
    FOREIGN KEY (`book_id`)
    REFERENCES `bookstore`.`book` (`id`),
  CONSTRAINT `sale_client`
    FOREIGN KEY (`client_id`)
    REFERENCES `bookstore`.`client` (`id`)
    )ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `bookstore`.`insert` (
  `id` INT NOT NULL,
  `date` TIMESTAMP NOT NULL,
  `count` INT NOT NULL,
  `book_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `insert_book`
    FOREIGN KEY (`book_id`)
    REFERENCES `bookstore`.`book` (`id`)
    ) ENGINE = InnoDB;


